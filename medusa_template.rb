# medusa_template.rb
project :test => :rspec,
  :orm => :mongoid,
  :renderer => :slim,
  :script => :jquery,
  :adapter => :mongo,
  :bundle => true

generate :model, "page title:string, :text"
generate :model, "cell content:text"
generate :controller, "pages get:name get:new post:new post:edit"
#require_dependencies 'nokogiri'

git :init
git :add, "."
git :commit, "Initial commit"

inject_into_file "app/models/page", "#Hello", :after => "end\n"
rake "ar:create ar:migrate"
initializer :test, "# Example"

app :testapp do
  generate :controller, "users get:index"
end
git :add, "."
git :commit, "second commit"
